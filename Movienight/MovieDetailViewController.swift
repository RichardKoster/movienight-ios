//
//  MovieDetailViewController.swift
//  Movienight
//
//  Created by Richard Köster on 12/11/2015.
//  Copyright © 2015 RichardKoster. All rights reserved.
//

import Foundation

class MovieDetailViewController: UIViewController{
    
    @IBOutlet var posterImageView:UIImageView!
    @IBOutlet var titleLabel:UILabel!
    @IBOutlet var overViewLabel:UILabel!
    @IBOutlet var contentContainerView:UIView!
    
    var movie:Movie? {
        didSet{
            populate()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        let navBounds = self.navigationController!.navigationBar.bounds
        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light))
        visualEffectView.frame = navBounds
        self.navigationController!.navigationBar.insertSubview(visualEffectView, atIndex: 0)
        
        let statusBarVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Dark))
        statusBarVisualEffectView.frame = UIApplication.sharedApplication().statusBarFrame
        self.view.addSubview(statusBarVisualEffectView)
    }
    
    func populate(){
        
    }
    
    override func viewDidAppear(animated: Bool) {
        let posterUrl = NSURL(string: movie!.getMoviePosterWithSize(Movie.ImageSize.ORIGINAL))
        posterImageView.sd_setImageWithURL(posterUrl)
        titleLabel.text = movie!.movieTitle
        overViewLabel.text = movie!.overview
        self.navigationController!.navigationItem.title = movie?.movieTitle
        contentContainerView.setNeedsLayout()
        contentContainerView.layoutIfNeeded()
    
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = contentContainerView.bounds
        gradient.colors = [
            UIColor(red: 0.07, green: 0.08, blue: 0.08, alpha: 0.0).CGColor,
            UIColor(red: 0.07, green: 0.08, blue: 0.08, alpha: 0.6).CGColor,
            UIColor(red: 0.07, green: 0.08, blue: 0.08, alpha: 0.8).CGColor,
            UIColor(red: 0.07, green: 0.08, blue: 0.08, alpha: 1).CGColor
        ]
        contentContainerView.layer.insertSublayer(gradient, atIndex: 0)
    }
    
}
