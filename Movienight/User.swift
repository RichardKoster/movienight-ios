//
//  User.swift
//  Movienight
//
//  Created by Richard Köster on 11/11/2015.
//  Copyright © 2015 RichardKoster. All rights reserved.
//

import Foundation

class User: NSObject{
    
    var username:String!
    var name:String!
    var location:String!
    var avatarUrl:String!
    var coverUrl:String!
    
    func getUser()->User{
        let defs = NSUserDefaults.standardUserDefaults()
        self.username = defs.stringForKey(UserKeys.username)
        self.name = defs.stringForKey(UserKeys.name)
        self.location = defs.stringForKey(UserKeys.location)
        self.avatarUrl = defs.stringForKey(UserKeys.avatarUrl)
        self.coverUrl = defs.stringForKey(UserKeys.coverImage)
        return self
    }
}
