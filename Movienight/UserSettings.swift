//
//  UserSettings.swift
//  Movienight
//
//  Created by Richard Köster on 12/11/2015.
//  Copyright © 2015 RichardKoster. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserSettings: NSObject {
    
    var username:String!
    var name:String!
    var location:String!
    var avatarUrl:String!
    var coverUrl:String!
    
    init(userObject:JSON) {
        username = userObject["user"]["username"].string!
        if let n = userObject["user"]["name"].string {
            name = n
        }
        if let l = userObject["user"]["location"].string {
            location = l
        }
        avatarUrl = userObject["user"]["images"]["avatar"]["full"].string!
        let cover:String! = userObject["account"]["cover_image"].string
        if let url = cover{
            coverUrl = url
        }
    }
}