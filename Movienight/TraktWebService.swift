//
//  TraktWebService.swift
//  TrendingMovies
//
//  Created by Richard Köster on 07/11/2015.
//  Copyright © 2015 RichardKoster. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol WebServiceDelegate{
    func onTrendingMoviesRetrieved(movies:[JSON])
    func onPopularMoviesRetrieved(movies:[JSON])
    func onBeingWatchedMoviesRetrieved(movies:[JSON])
}

protocol AuthDelegate{
    func didReceiveAccessToken(accessToken:JSON)
}

protocol UserListsDelegate{
    func didReceiveWatchList(movies:[JSON])
    func didReceiveWatched(movies:[JSON])
}

protocol UserSettingsDelegate{
    func didReceiveUserSettings(user:JSON)
}

protocol FriendsDelegate{
    func didReceiveFriends(friends:JSON)
    func didReceiveFollowers(followers:JSON)
    func didReceiveFollowing(following:JSON)
}

class TraktWebService: NSObject{
    
   
    var client_id:String {
        get{
            return "c07996688d0d2fb1db6637829f26d6abad5237f2c354a1e17039a2f5b9f09259"
        }
    }
    var client_secret:String {
        get{
            return "84913feaea3e625b593b2cd551b2c1474d7ff58c67b2c98e460e634c6dda1cea"
        }
    }
    var return_url:String{
        get{
            return "movienight://com.richardkoster.movienight/login"
        }
    }
    
    var delegate:WebServiceDelegate!
    var authDelegate:AuthDelegate!
    var userSettingsDelegate:UserSettingsDelegate!
    var userListsDelegate:UserListsDelegate!
    var friendsDelegate:FriendsDelegate!
    
    let headers = [
        "Content-Type": "application/json",
        "trakt-api-version": "2",
        "trakt-api-key": "c07996688d0d2fb1db6637829f26d6abad5237f2c354a1e17039a2f5b9f09259",
    ]
    let parameters = ["extended":"full,images"]
    
    
    func getAccessToken(code:String){
        let request = NSMutableURLRequest(URL: NSURL(string: "https://api-v2launch.trakt.tv/oauth/token")!)
        let bodyDict:NSMutableDictionary = NSMutableDictionary()
        bodyDict.setObject(client_id, forKey: "client_id")
        bodyDict.setObject(code, forKey: "code")
        bodyDict.setObject(client_secret, forKey: "client_secret")
        bodyDict.setObject(return_url, forKey: "redirect_uri")
        bodyDict.setObject("authorization_code", forKey: "grant_type")
        var bodyData:NSData? = NSData()
        do {
            bodyData = try NSJSONSerialization.dataWithJSONObject(bodyDict, options: NSJSONWritingOptions.PrettyPrinted)
        } catch _ {
            NSLog("Oh noes")
        }
        request.HTTPMethod = "POST"
        request.HTTPBody = bodyData
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        Alamofire.request(request)
        .responseJSON { response in
            let atJson:JSON? = JSON(response.result.value!)
            if let result = atJson {
                self.authDelegate.didReceiveAccessToken(result)
            }
        }
    }
    
    func getTrendingMovies(){
        Alamofire.request(.GET, "https://api-v2launch.trakt.tv/movies/trending", parameters:parameters, headers:headers)
            .responseJSON { response in
                if let value: AnyObject = response.result.value{
                    var movies = [JSON]()
                    let post = JSON(value)
                    for (_,subJson):(String, JSON) in post {
                        movies.append(subJson["movie"])
                    }
                    self.delegate.onTrendingMoviesRetrieved(movies)
                }
        }
    }
    
    func getPopularMovies(){
        Alamofire.request(.GET, "https://api-v2launch.trakt.tv/movies/popular", parameters:parameters, headers:headers)
            .responseJSON { response in
                if let value: AnyObject = response.result.value{
                    let post = JSON(value)
                    self.delegate.onPopularMoviesRetrieved(post.array!)
                }
        }
    }
    
    func getBeingWatchedMovies(){
        Alamofire.request(.GET, "https://api-v2launch.trakt.tv/movies/watched/weekly", parameters:parameters, headers:headers)
            .responseJSON { response in
                if let value: AnyObject = response.result.value{
                    var movies = [JSON]()
                    let post = JSON(value)
                    for (_,subJson):(String, JSON) in post {
                        movies.append(subJson["movie"])
                    }
                    self.delegate.onBeingWatchedMoviesRetrieved(movies)
                }
        }
    }
    
    func getUserSettings(token:AccessToken){
        let authHeaders = [
            "Content-Type": "application/json",
            "trakt-api-version": "2",
            "trakt-api-key": "c07996688d0d2fb1db6637829f26d6abad5237f2c354a1e17039a2f5b9f09259",
            "Authorization": "Bearer "+token.accessToken
        ]
        Alamofire.request(.GET, "https://api-v2launch.trakt.tv/users/settings", parameters:parameters, headers:authHeaders)
            .responseJSON { response in
                let json:JSON? = JSON(response.result.value!)
                if let result = json {
                    self.userSettingsDelegate.didReceiveUserSettings(result)
                }
        }
    }
    
    func getWatchlistForUser(user:String){
        Alamofire.request(.GET, "https://api-v2launch.trakt.tv/users/"+user+"/watchlist/movies", parameters:parameters, headers:headers)
            .responseJSON { response in
                if let value: AnyObject = response.result.value{
                    var movies = [JSON]()
                    let post = JSON(value)
                    for (_,subJson):(String, JSON) in post {
                        movies.append(subJson["movie"])
                    }
                    self.userListsDelegate.didReceiveWatchList(movies)
                }
        }
    }
    
    func getWatchedForUser(user:String){
        Alamofire.request(.GET, "https://api-v2launch.trakt.tv/users/"+user+"/watched/movies", parameters:parameters, headers:headers)
            .responseJSON { response in
                if let value: AnyObject = response.result.value{
                    var movies = [JSON]()
                    let post = JSON(value)
                    for (_,subJson):(String, JSON) in post {
                        movies.append(subJson["movie"])
                    }
                    self.userListsDelegate.didReceiveWatched(movies)
                }
        }
    }
    
    func getFriendsOfUser(username:String){
        Alamofire.request(.GET, "https://api-v2launch.trakt.tv/users/"+username+"/friends", parameters:parameters, headers:headers)
            .responseJSON { response in
                let json:JSON? = JSON(response.result.value!)
                if let result = json {
                    self.friendsDelegate.didReceiveFriends(result)
                }
        }
        
        Alamofire.request(.GET, "https://api-v2launch.trakt.tv/users/"+username+"/followers", parameters:parameters, headers:headers)
            .responseJSON { response in
                let json:JSON? = JSON(response.result.value!)
                if let result = json {
                    self.friendsDelegate.didReceiveFollowers(result)
                }
        }
        Alamofire.request(.GET, "https://api-v2launch.trakt.tv/users/"+username+"/following", parameters:parameters, headers:headers)
            .responseJSON { response in
                let json:JSON? = JSON(response.result.value!)
                if let result = json {
                    self.friendsDelegate.didReceiveFollowing(result)
                }
        }
    }
    
}


