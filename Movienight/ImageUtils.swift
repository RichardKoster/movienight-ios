//
//  ImageUtils.swift
//  Movienight
//
//  Created by Richard Köster on 12/11/2015.
//  Copyright © 2015 RichardKoster. All rights reserved.
//

import Foundation

class ImageUtils {
    
    static func imageFromColor(color:UIColor) ->UIImage{
        let rect = CGRectMake(0, 0, 1, 1)
        UIGraphicsBeginImageContext(rect.size)
        let context:CGContextRef = UIGraphicsGetCurrentContext()!
        CGContextSetFillColorWithColor(context, color.CGColor)
        CGContextFillRect(context, rect)
        let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}