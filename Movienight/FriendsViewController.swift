//
//  FriendsViewController.swift
//  Movienight
//
//  Created by Richard Köster on 11/11/2015.
//  Copyright © 2015 RichardKoster. All rights reserved.
//

import Foundation
import SwiftyJSON

class FriendsCell : UITableViewCell {
    @IBOutlet var avatarImageView:UIImageView!
    @IBOutlet var usernameLabel:UILabel!
    @IBOutlet var nameLabel:UILabel!
}

class FriendsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FriendsDelegate {
    
    @IBOutlet var segmentedController:UISegmentedControl!
    @IBOutlet var tableView:UITableView!
    
    @IBAction func segmentControlChanged(sender:AnyObject){
        switch segmentedController.selectedSegmentIndex {
        case 0:
            dataset = friends
            tableView.reloadData()
            break;
        case 1:
            dataset = followers
            tableView.reloadData()
            break;
        case 2:
            dataset = following
            tableView.reloadData()
            break;
        default:
            dataset = friends
            tableView.reloadData()
            break;
        }
    }
    
    var webservice:TraktWebService!
    
    var dataset:JSON!
    
    var friends:JSON!
    var followers:JSON!
    var following:JSON!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        webservice = TraktWebService()
        webservice.friendsDelegate = self
        webservice.getFriendsOfUser(User().getUser().username)
    }
    
    func didReceiveFriends(friends: JSON) {
        self.friends = friends
        dataset = friends
        tableView.reloadData()
    }
    
    func didReceiveFollowers(followers: JSON) {
        self.followers = followers
    }
    
    func didReceiveFollowing(following: JSON) {
        self.following = following
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataset != nil {
            return dataset.count

        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:FriendsCell = tableView.dequeueReusableCellWithIdentifier("FriendsCell") as! FriendsCell
        let user = UserSettings(userObject: dataset.array![indexPath.row])
        let avatarURl = NSURL(string: user.avatarUrl)
        cell.avatarImageView.sd_setImageWithURL(avatarURl)
        cell.usernameLabel.text = user.username
        cell.nameLabel.text = user.name
        return cell;
    }
    
}
