//
//  Movie.swift
//  MovieNight
//
//  Created by Richard Köster on 06/11/2015.
//  Copyright © 2015 RichardKoster. All rights reserved.
//

import Foundation
import SwiftyJSON

class Movie : NSObject {
    
    enum ImageSize{
        case THUMB, MEDIUM, ORIGINAL
    }
    
    var movieTitle:String
    var year:Int
    var ids:NSDictionary
    var tagline:String
    var overview:String
    var runtime:Int
    var trailerUrl:String
    var rating:Float
    var votes:Int
    var genres:[String]
    var images:NSDictionary
    
    
    init(movieObject:JSON){
        self.movieTitle = movieObject["title"].string!
        self.year = movieObject["year"].int!
        self.ids = movieObject["ids"].dictionaryObject!
        self.tagline = movieObject["tagline"].string!
        self.overview = movieObject["overview"].string!
        self.runtime = movieObject["runtime"].int!
        if let trailerStr = movieObject["trailer"].string{
            self.trailerUrl = trailerStr
        }
        else{
            self.trailerUrl = ""
        }
        self.rating = movieObject["rating"].float!
        self.votes = movieObject["votes"].int!
        self.genres = movieObject["genres"].arrayValue.map{$0.string!}
        self.images = movieObject["images"].dictionaryObject!
        super.init()
    }
    
    func getMoviePosterWithSize(size:ImageSize)->String{
        let posterDict = images.objectForKey("poster") as! NSDictionary
        switch size {
        case .THUMB:
            return posterDict.valueForKey("thumb") as! String
        case .MEDIUM:
            return posterDict.valueForKey("medium") as! String
        case .ORIGINAL:
            return posterDict.valueForKey("full") as! String
        }
    }
    
    func getMovieFanartWithSize(size:ImageSize)->String{
        let dict = images.objectForKey("fanart") as! NSDictionary
            switch size {
            case .THUMB:
                if let url = dict.valueForKey("thumb") as? String{
                    return url
                }
                return ""
            case .MEDIUM:
                if let url = dict.valueForKey("medium") as? String{
                    return url
                }
                return ""
            case .ORIGINAL:
                if let url = dict.valueForKey("full") as? String{
                    return url
                }
                return ""
            }
    }
}
