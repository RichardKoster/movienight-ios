//
//  KenBurnsView.swift
//  Movienight
//
//  Created by Richard Köster on 10/11/2015.
//  Copyright © 2015 RichardKoster. All rights reserved.
//

import Foundation

protocol KenBurnsViewDelegate{
    func didShowImage(image:UIImage, atIndex:Int, kenBurns:KenBurnsView)
    func didFinishAllImages(images:NSArray!, kenBurns:KenBurnsView)
}

enum SourceMode {
    case SourceModePaths
    case SourceModeImages
}

enum ZoomMode {
    case ZoomModeIn, ZoomModeOut, ZoomModeRandom
}

// Private interface
class KenBurnsView: UIView{
    
    var delegate:KenBurnsViewDelegate!
    var currentImageIndex = -1
    var zoomMode:ZoomMode!

    var imagesArray:NSMutableArray!
    var nextImageTimer:NSTimer!
    var showImageDuration:Float!
    var shouldLoop:Bool!
    var isLandscape:Bool!
    var sourceMode:SourceMode!
    
    let enlargeRatio = 1.1
    let imageBuffer = 3
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setup()
    }
    
    override func awakeFromNib() {
        self.setup()
    }

    func setup(){
        self.backgroundColor = UIColor.clearColor()
        self.layer.masksToBounds = true
        self.zoomMode = ZoomMode.ZoomModeIn
    }
    
    func animateWitImagePaths(imagePaths:NSArray, duration:Float, initialDelay:Float, loop:Bool, isLandscape:Bool){
        sourceMode = SourceMode.SourceModePaths
        startAnimationsWithData(imagePaths, duration: duration, initialDelay: initialDelay, loop: loop, isLandscape: isLandscape)
    }
    
    func animateWitImages(images:NSArray, duration:Float, initialDelay:Float, loop:Bool, isLandscape:Bool){
        sourceMode = SourceMode.SourceModeImages
        startAnimationsWithData(images, duration: duration, initialDelay:initialDelay, loop:loop, isLandscape: isLandscape)
    }
    
    func startAnimationsWithData(data:NSArray, duration:Float, initialDelay:Float, loop:Bool, isLandscape:Bool){
        self.imagesArray = data as! NSMutableArray
        self.showImageDuration = duration
        self.shouldLoop = loop
        self.isLandscape = isLandscape
        self.currentImageIndex = -1
        
        let initDelInt = Int64(initialDelay)
        let nsecPerSec = Int64(bitPattern: NSEC_PER_SEC)
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, initDelInt*nsecPerSec), dispatch_get_main_queue()) { () -> Void in
            self.nextImageTimer = NSTimer.scheduledTimerWithTimeInterval(NSTimeInterval(duration), target: self, selector: "nextImage", userInfo: nil, repeats: true)
            self.nextImageTimer.fire()
        }
    }
    
    func stopAnimation(){
        self.layer.removeAllAnimations()
        
        if self.nextImageTimer != nil && self.nextImageTimer.valid {
            self.nextImageTimer.invalidate()
            self.nextImageTimer = nil
        }
    }
    
    func addImage(image:UIImage){
        self.imagesArray.addObject(image)
    }

    func images() ->NSArray {
        return self.imagesArray
    }
    
    func currentImage() ->UIImage{
        var image:UIImage! = UIImage()
        var imageInfo:AnyObject
        if imagesArray.count > 0 {
            imageInfo = imagesArray[min(imagesArray.count - 1, max(self.currentImageIndex, 0))]
            switch self.sourceMode as SourceMode {
            case SourceMode.SourceModeImages:
                image = imageInfo as! UIImage
                break
            case SourceMode.SourceModePaths:
                if imageInfo as! String != "" {
                    let url = NSURL(string: imageInfo as! String)
                    image = UIImage(data: NSData(contentsOfURL: url!)!)
                }
                else{
                    image = UIImage(contentsOfFile: NSBundle.mainBundle().pathForResource("placeholder_backdrop", ofType: "png")!)
                }
                //image = UIImage(contentsOfFile: imageInfo as! String)
                break;
            }
        }

        return image
    }
    
    func nextImage(){
        self.currentImageIndex++
        
        let image:UIImage = self.currentImage()
        var imageView:UIImageView!
        
        var originX:CGFloat = -1
        var originY:CGFloat = -1
        var zoomInX:CGFloat = -1
        var zoomInY:CGFloat = -1
        var moveX:CGFloat = -1
        var moveY:CGFloat = -1
        
        var frameWidth:CGFloat!
        var frameHeight:CGFloat!
        frameWidth = self.isLandscape == false ? self.bounds.size.width : self.bounds.size.height
        frameHeight = self.isLandscape == false ? self.bounds.size.height : self.bounds.size.width
              
        let resizeRatio:CGFloat = getResizeRatioFromImage(image, frameWidth:frameWidth, frameHeight:frameHeight)
        
        let optimusWidth = (image.size.width*resizeRatio) * CGFloat(enlargeRatio)
        let optimusHeight = (image.size.height*resizeRatio) * CGFloat(enlargeRatio)
        
        imageView = UIImageView.init(frame: CGRectMake(0, 0, optimusWidth, optimusHeight))
        imageView.backgroundColor = UIColor.redColor()
        
        let maxMoveX = optimusWidth - frameWidth
        let maxMoveY = optimusHeight - frameHeight
        let rotation:CGFloat = CGFloat((arc4random() % 9) / 100)
        let moveType = arc4random() % 4
        switch moveType{
        case 0:
            originX = 0;
            originY = 0;
            zoomInX = 1.25;
            zoomInY = 1.25;
            moveX   = -maxMoveX;
            moveY   = -maxMoveY;
            break;
            
        case 1:
            originX = 0;
            originY = frameHeight - optimusHeight;
            zoomInX = 1.10;
            zoomInY = 1.10;
            moveX   = -maxMoveX;
            moveY   = maxMoveY;
            break;
            
        case 2:
            originX = frameWidth - optimusWidth;
            originY = 0;
            zoomInX = 1.30;
            zoomInY = 1.30;
            moveX   = maxMoveX;
            moveY   = -maxMoveY;
            break;
            
        case 3:
            originX = frameWidth - optimusWidth;
            originY = frameHeight - optimusHeight;
            zoomInX = 1.20;
            zoomInY = 1.20;
            moveX   = maxMoveX;
            moveY   = maxMoveY;
            break;
            
        default:
            NSLog("Unknown random number found in JBKenBurnsView _animate");
            originX = 0;
            originY = 0;
            zoomInX = 1;
            zoomInY = 1;
            moveX   = -maxMoveX;
            moveY   = -maxMoveY;
            break;

        }
        
        let picLayer = CALayer()
        picLayer.contents = image.CGImage
        picLayer.anchorPoint = CGPointMake(0, 0)
        picLayer.bounds = CGRectMake(0, 0, optimusWidth, optimusHeight);
        picLayer.position = CGPointMake(originX, originY);
        
        imageView.layer.addSublayer(picLayer)
        
        let animation = CATransition()
        animation.duration = 1
        animation.type = kCATransitionFade
        self.layer.addAnimation(animation, forKey: nil)
        
        if self.subviews.count > 0 {
            let oldImageView = self.subviews[0]
            oldImageView.removeFromSuperview()
        }
        
        self.addSubview(imageView)
        
        let rotate = CGAffineTransformMakeRotation(rotation)
        let moveRight = CGAffineTransformMakeTranslation(moveX, moveY)
        let combo1 = CGAffineTransformConcat(rotate, moveRight)
        let zoomIn = CGAffineTransformMakeScale(zoomInX, zoomInY)
        let transform = CGAffineTransformConcat(zoomIn, combo1)
        
        let zoomedTransform = transform
        let standardTransform = CGAffineTransformIdentity
        
        var startTransform = CGAffineTransformIdentity
        var finishTransform = CGAffineTransformIdentity
        
        switch self.zoomMode as ZoomMode {
        case ZoomMode.ZoomModeIn:
            startTransform = standardTransform;
            finishTransform = zoomedTransform;
            break;
        case ZoomMode.ZoomModeOut:
            startTransform = zoomedTransform;
            finishTransform = standardTransform;
            break;
        case ZoomMode.ZoomModeRandom:
            if randomBool() == true {
                startTransform = zoomedTransform;
                finishTransform = standardTransform;
            }
            else {
                startTransform = standardTransform;
                finishTransform = zoomedTransform;
            }
            break;
        }
        
        imageView.transform = startTransform
        UIView.animateWithDuration(NSTimeInterval(self.showImageDuration+2), delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
            imageView.transform = finishTransform
            }) { (Bool) -> Void in
                
        }
        
        
        if self.currentImageIndex == self.imagesArray.count - 1 {
            if shouldLoop == true { self.currentImageIndex = -1 }
            else { self.nextImageTimer.invalidate() }
        }
    }
    
    func getResizeRatioFromImage(image:UIImage, frameWidth:CGFloat, frameHeight:CGFloat) -> CGFloat{
        var resizeRatio:CGFloat = -1
        var widthDiff:CGFloat = -1
        var heightDiff:CGFloat = -1
        

        if image.size.width > frameWidth {
            widthDiff  = image.size.width - frameWidth;
            
            if image.size.height > frameHeight {
                heightDiff = image.size.height - frameHeight;
                
                resizeRatio = widthDiff > heightDiff ? frameHeight / image.size.height : frameWidth / image.size.width
            }
            else {
                heightDiff = frameHeight - image.size.height;
                
                resizeRatio = widthDiff < heightDiff ? frameWidth / image.size.width : self.bounds.size.height / image.size.height
            }
        }
        else{
            widthDiff  = frameWidth - image.size.width;
            
            if image.size.height > frameHeight {
                heightDiff = image.size.height - frameHeight;
                
                resizeRatio = widthDiff > heightDiff ? image.size.height / frameHeight : frameWidth / image.size.width
            }
            else{
                heightDiff = frameHeight - image.size.height;
                
                resizeRatio = widthDiff < heightDiff ? frameWidth / image.size.width : frameHeight / image.size.height
            }
        }

        return resizeRatio
    }
    
    func randomBool()->Bool{
        return arc4random_uniform(100) < 100
    }
    
}
