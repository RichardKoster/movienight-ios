//
//  WebViewController.swift
//  Movienight
//
//  Created by Richard Köster on 10/11/2015.
//  Copyright © 2015 RichardKoster. All rights reserved.
//

import Foundation
import SwiftyJSON

enum AuthKeys{
    static let accessToken = "access_token"
    static let createdAt = "created_at"
    static let expiresIn = "expires_in"
    static let refreshToken = "refresh_token"
    static let scope = "scope"
    static let tokenType = "token_type"
}

class WebViewController: UIViewController, UIWebViewDelegate, AuthDelegate {
    
    @IBOutlet var webview:UIWebView!
    var webservice:TraktWebService!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webservice = TraktWebService()
        let url = "https://api-v2launch.trakt.tv/oauth/authorize?response_type=code&client_id="+webservice.client_id+"&redirect_uri="+webservice.return_url
        
        webview.loadRequest(NSURLRequest(URL: NSURL(string: url)!))
        webview.delegate = self
        webservice.authDelegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let url = request.URL
        let host = url!.host
        let path = url!.path
        let query = url!.query
        
        if host == "com.richardkoster.movienight" && path == "/login" {
            let queryComps = query?.componentsSeparatedByString("=")
            let code = queryComps![1]
            webservice.getAccessToken(code)
        }
        return true
    }
    
    func didReceiveAccessToken(accessToken: JSON) {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setValue(accessToken["access_token"].string!, forKey: AuthKeys.accessToken)
        defaults.setValue(accessToken["created_at"].int!, forKey: AuthKeys.createdAt)
        defaults.setValue(accessToken["expires_in"].int!, forKey: AuthKeys.expiresIn)
        defaults.setValue(accessToken["refresh_token"].string!, forKey: AuthKeys.refreshToken)
        defaults.setValue(accessToken["scope"].string!, forKey: AuthKeys.scope)
        defaults.setValue(accessToken["token_type"].string!, forKey: AuthKeys.tokenType)
        defaults.synchronize()
        NSNotificationCenter.defaultCenter().postNotificationName("ModalViewControllerDismissed", object: nil)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}
