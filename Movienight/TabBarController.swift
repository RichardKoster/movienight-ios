//
//  TabBarController.swift
//  Movienight
//
//  Created by Richard Köster on 11/11/2015.
//  Copyright © 2015 RichardKoster. All rights reserved.
//

import Foundation
import SwiftyJSON

class TabBarController:UITabBarController, UserSettingsDelegate, UISplitViewControllerDelegate{
    
    var webservice:TraktWebService!
    @IBOutlet var tabbar:UITabBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webservice = TraktWebService()
        webservice.userSettingsDelegate = self
        let splitViewController = self.viewControllers![1] as! UISplitViewController
        let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
        navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem()
        splitViewController.delegate = self
        splitViewController.collapseSecondaryViewController(navigationController, forSplitViewController: splitViewController)
    }
    
    override func viewDidAppear(animated: Bool) {
        let accessToken = AccessToken().getAccessToken()
        if accessToken.accessToken == nil {
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "didDismissLoginView", name: "DismissedLoginView", object: nil)
            performSegueWithIdentifier("showLoginView", sender: self)
        }
        else if User().getUser().username == nil {
            webservice.getUserSettings(accessToken)
        }
        let movienightViewController = self.viewControllers![0] as! MovienightViewController
        movienightViewController.populateView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func didDismissLoginView(){
        NSLog("We are in bizznizz now!")
    }
    
    func didReceiveUserSettings(user: JSON) {
        let defs = NSUserDefaults.standardUserDefaults()
        defs.setObject(user["user"]["username"].string!, forKey: UserKeys.username)
        defs.setObject(user["user"]["name"].string!, forKey: UserKeys.name)
        defs.setObject(user["user"]["location"].string!, forKey: UserKeys.location)
        defs.setObject(user["user"]["images"]["avatar"]["full"].string!, forKey: UserKeys.avatarUrl)
        let coverImage:String? = user["account"]["cover_image"].string
        if let url = coverImage{
            defs.setObject(url, forKey: UserKeys.coverImage)
        }
        defs.synchronize()
    }
    
    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController:UIViewController, ontoPrimaryViewController primaryViewController:UIViewController) -> Bool {
        guard let secondaryAsNavController = secondaryViewController as? UINavigationController else { return false }
        guard let topAsDetailController = secondaryAsNavController.topViewController as? MovieDetailViewController else { return false }
        if topAsDetailController.movie == nil {
            // Return true to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
            return true
        }
        return false
    }
    
}
