//
//  LoginViewController.swift
//  Movienight
//
//  Created by Richard Köster on 10/11/2015.
//  Copyright © 2015 RichardKoster. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

enum UserKeys{
    static let username = "username"
    static let name = "name"
    static let location = "location"
    static let avatarUrl = "avatarUrl"
    static let coverImage = "coverImage"
}

class LoginViewController:UIViewController, WebServiceDelegate, UserSettingsDelegate{
    
    @IBOutlet weak var fanartView: KenBurnsView!
    @IBOutlet var loginButton:UIButton!
    
    var webservice:TraktWebService!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webservice = TraktWebService()
        webservice.delegate = self
        webservice.userSettingsDelegate = self
        webservice.getTrendingMovies()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func onTrendingMoviesRetrieved(movies: [JSON]) {
        let fanartPaths:NSMutableArray! = NSMutableArray()
        for movie in movies{
            fanartPaths.addObject(Movie(movieObject: movie).getMovieFanartWithSize(Movie.ImageSize.ORIGINAL))
        }
        fanartView.animateWitImagePaths(fanartPaths, duration: 15, initialDelay: 0, loop: true, isLandscape: false)
    }
    
    func onPopularMoviesRetrieved(movies: [JSON]) {
        
    }
    
    func onBeingWatchedMoviesRetrieved(movies: [JSON]) {
        
    }
    
    @IBAction func didClickOnLogin(sender:AnyObject){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "didDismissWebViewController", name: "ModalViewControllerDismissed", object: nil)
        performSegueWithIdentifier("showWebView", sender: self)
    }
    
    func didDismissWebViewController(){
        let accessToken = AccessToken().getAccessToken()
        webservice.getUserSettings(accessToken)
        NSNotificationCenter.defaultCenter().postNotificationName("DismissedLoginView", object: nil)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func didReceiveUserSettings(user: JSON) {
        let defs = NSUserDefaults.standardUserDefaults()
        defs.setObject(user["user"]["username"].string!, forKey: UserKeys.username)
        defs.setObject(user["user"]["name"].string!, forKey: UserKeys.name)
        defs.setObject(user["user"]["location"].string!, forKey: UserKeys.location)
        defs.setObject(user["user"]["images"]["avatar"]["full"].string!, forKey: UserKeys.avatarUrl)
        let coverUrl:String? = user["account"]["cover_image"].string
        if let url = coverUrl {
            defs.setObject(url, forKey: UserKeys.coverImage)
        }
        defs.synchronize()
    }
    
}
