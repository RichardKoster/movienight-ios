//
//  AccessToken.swift
//  Movienight
//
//  Created by Richard Köster on 11/11/2015.
//  Copyright © 2015 RichardKoster. All rights reserved.
//

import Foundation

class AccessToken: NSObject {
    
    var accessToken:String!
    var tokenType:String!
    var expiresIn:Int!
    var refreshToken:String!
    var scope:String!
    
    func getAccessToken()->AccessToken{
        let defs = NSUserDefaults.standardUserDefaults()
        if let accessToken = defs.stringForKey(AuthKeys.accessToken){
            self.accessToken = accessToken
        }
        if let tokenType = defs.stringForKey(AuthKeys.tokenType){
            self.tokenType = tokenType
        }
        let expiresInInt:Int? = defs.valueForKey(AuthKeys.expiresIn)?.integerValue
        if let expiresIn = expiresInInt{
            self.expiresIn = expiresIn
        }
        if let refreshToken = defs.stringForKey(AuthKeys.refreshToken){
            self.refreshToken = refreshToken
        }
        if let scope = defs.stringForKey(AuthKeys.scope){
            self.scope = scope
        }
        return self
    }
}