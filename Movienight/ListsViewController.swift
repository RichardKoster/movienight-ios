//
//  SecondViewController.swift
//  Movienight
//
//  Created by Richard Köster on 09/11/2015.
//  Copyright © 2015 RichardKoster. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class MovieCollectionCell: UICollectionViewCell{
    var showingOptions:Bool!
    @IBOutlet var posterImageView:UIImageView!
    @IBOutlet var optionsView:UIView!
    @IBOutlet var addButton:UIButton!
}

class ListsViewController: UIViewController, UserListsDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {
    
    @IBOutlet var segmentedControl:UISegmentedControl!
    @IBOutlet var collectionView:UICollectionView!
    
    @IBAction func didTapAddButton(sender:AnyObject){
        
    }
    
    @IBAction func onSegmentedControlChanged(){
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            dataset = watchlist
            collectionView.reloadData()
            self.title = "Watchlist"
            break
        case 1:
            dataset = watched
            collectionView.reloadData()
            self.title = "Watched"
            break
        default:
            dataset = watchlist
            collectionView.reloadData()
        }
    }
    
    var currentCellOpened:MovieCollectionCell!
    var isOptionsOpen:Bool!
    
    var tappedMovie:Movie!
    
    var dataset = [JSON]()
    var watchlist = [JSON]()
    var watched = [JSON]()

    var webservice:TraktWebService!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        styleNavBar()
        webservice = TraktWebService()
        webservice.userListsDelegate = self
        let user = User().getUser()
        webservice.getWatchedForUser(user.username)
        webservice.getWatchlistForUser(user.username)
        collectionView.delegate = self
        collectionView.dataSource = self
        
        let lpRec = UILongPressGestureRecognizer(target: self, action: "handleLongPressGesture:")
        lpRec.minimumPressDuration = 0.5
        lpRec.delaysTouchesBegan = true
        collectionView.addGestureRecognizer(lpRec)
        
        let tapRec = UITapGestureRecognizer(target: self, action: "handleTapGesture:")
        tapRec.numberOfTapsRequired = 1
        tapRec.delaysTouchesBegan = true
        collectionView.addGestureRecognizer(tapRec)
        isOptionsOpen = false
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func styleNavBar(){
        self.navigationController!.navigationBar.setBackgroundImage(ImageUtils.imageFromColor(UIColor(red: 0, green: 0.59, blue: 0.53, alpha: 1)), forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.translucent = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func didReceiveWatched(movies: [JSON]) {
        watched = movies
    }
    
    func didReceiveWatchList(movies: [JSON]) {
        watchlist = movies
        dataset = watchlist
        self.title = "Watchlist"
        collectionView.reloadData()
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("MovieCollectionViewCell", forIndexPath: indexPath) as! MovieCollectionCell
        let movie = Movie(movieObject: dataset[indexPath.row])
        let url = NSURL(string: movie.getMoviePosterWithSize(Movie.ImageSize.THUMB))
        cell.posterImageView.sd_setImageWithURL(url)
        cell.optionsView.hidden = true
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataset.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width = collectionView.frame.size.width/3 - 2/3
        let height = width*1.5
        return CGSize(width: width, height: height)
    }
    
    func handleTapGesture(gestureRecognizer:UITapGestureRecognizer){
        NSLog("tap")
        if isOptionsOpen == true {
            currentCellOpened.optionsView.hidden = true
            setBlur(currentCellOpened, state: false)
            isOptionsOpen = false
        }
        else{
            let p = gestureRecognizer.locationInView(collectionView)
            let indexPath:NSIndexPath? = collectionView.indexPathForItemAtPoint(p)
            if let ip = indexPath{
                tappedMovie = Movie(movieObject: dataset[ip.row])
                performSegueWithIdentifier("showMovieDetail", sender: self)
            }
        }
    }
    
    func handleLongPressGesture(gestureRecognizer:UITapGestureRecognizer){
        let p = gestureRecognizer.locationInView(collectionView)
        let indexPath:NSIndexPath? = collectionView.indexPathForItemAtPoint(p)
        if let ip = indexPath {
            let cell:MovieCollectionCell = collectionView.cellForItemAtIndexPath(ip) as! MovieCollectionCell
            if isOptionsOpen == true {
                currentCellOpened.optionsView.hidden = true
                setBlur(currentCellOpened, state:false)
                isOptionsOpen = false
            }
            currentCellOpened = cell
            isOptionsOpen = true
            setBlur(cell, state: true)
            cell.optionsView.hidden = false
        }
    }
    
    func setBlur(cell:MovieCollectionCell, state:Bool){
        if state == true {
            let fxView = UIVisualEffectView(effect: UIBlurEffect(style: .Dark))
            fxView.frame = cell.optionsView.bounds
            fxView.tag = 200
            cell.optionsView.insertSubview(fxView, belowSubview: cell.addButton)
            return
        }
        
        for subView in cell.optionsView.subviews {
            if subView.tag == 200 {
                subView.removeFromSuperview()
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showMovieDetail" {
            let detailController = (segue.destinationViewController as! UINavigationController).topViewController as! MovieDetailViewController
            detailController.movie = tappedMovie
            detailController.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
            detailController.navigationItem.leftItemsSupplementBackButton = true
        }
    }
}

